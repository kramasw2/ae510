%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code for Shock tube spatial distribution of properties       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc;
clear all;
close all;
%% Constants/Values

gam = 1.4;
T4 = 2500;
P4 = 10;

P3_4 = 0.4;
a4 = 1002.25;

%% Function Definitions

u_a4 = @(x,t) (2/(gam - 1))*(1 + x./(t*a4))./(1 + 2/(gam - 1));

a_a4 = @(x,t) (2 - (gam - 1).*x./(a4*t))./(gam + 1);

T_T4 = @(x,t) a_a4(x,t).^2;

rho_rho4 = @(x,t) a_a4(x,t).^(2/(gam - 1));

P_P4 = @(x,t) a_a4(x,t).^(2*gam/(gam - 1));

%% Plots and Values

x = linspace(-1.002225, -0.26444)';

t = 1e-3;

figure(1)
plot(x, u_a4(x,t));
xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('u/a_4', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
grid on

figure(2)
plot(x, a_a4(x,t));
xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('a/a_4', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
grid on

figure(3)
plot(x, T_T4(x,t));
xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('T/T_4', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
grid on

figure(4)
plot(x, P_P4(x,t));
xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('P/P_4', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
grid on

figure(5)
plot(x, rho_rho4(x,t));
xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('\rho/\rho_4', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
grid on