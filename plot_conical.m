%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Program to plot deltac vs theta_s, Cp_c vs delta_c and
% M_c vs delta_c. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

d_theta = 0.2;           % Increment to vary theta_s 
dpsi = 0.4;
theta_s_min = 30;
theta_s_max = 69;

iterrange = theta_s_min:d_theta:theta_s_max;
iterrange = transpose(iterrange);   

s = size(iterrange);
dc = zeros(s);
cpc = zeros(s);
Mc = zeros(s);
Vals = zeros(s(1),4);


for k = 1:(length(iterrange))
    [dc(k,1), cpc(k,1), Mc(k,1)] = HW4_q1(iterrange(k), dpsi);
    Vals(k,1) = iterrange(k,1);
end

Vals(:,2) = dc;
Vals(:,3) = cpc;
Vals(:,4) = Mc;

% Shock Chart Data

sc_data = zeros(12,3);
sc_data(:,1) = [30;32;35;40;45;48;50;52;55;60;65;68]; % theta_s values
sc_data(:,2) = [0;11.5000000000000;16.5000000000000;22.5000000000000;...
    27.500000000000;30;31.2500000000000;33;35;38;40;40.7500000000000]; % delta_c values
sc_data(:,3) = [0;0.130000000000000;0.237500000000000;0.397500000000000;...
    0.560000000000000;0.640000000000000;0.695000000000000;...
    0.730000000000000;0.820000000000000;0.980000000000000;1.10000000000000;...
    1.16000000000000];

sc_data_dc = [0;14.5000000000000;19;22.2500000000000;26;28.5000000000000;...
    31.5000000000000;34.2500000000000;36.5000000000000;37.2500000000000;...
    38.5000000000000;39.5000000000000;40.5000000000000];

sc_data_Mc = [2;1.70000000000000;1.60000000000000;1.50000000000000;...
    1.40000000000000;1.30000000000000;1.20000000000000;1.10000000000000;1;...
    0.950000000000000;0.900000000000000;0.850000000000000;...
    0.750000000000000];

% D_c vs theta_c

figure(1)
plot(Vals(:,2), Vals(:,1))
xlabel('\delta_c', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
ylabel('\theta_s', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
hold on;
plot(sc_data(:,2), sc_data(:,1), 'rs');
grid on
legend('4th Order RK', 'NACA 1135 Data')


% Cp_c vs theta_c

figure(2)
plot(Vals(:,2), Vals(:,3))
xlabel('\delta_c', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('(C_p)_c', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
hold on;
plot(sc_data(:,2), sc_data(:,3), 'rs');
grid on
legend('4th Order RK', 'NACA 1135 Data')
hold on;    


% M_c vs theta_c

figure(3)
hold on;
plot(Vals(:,2), Vals(:,4))
xlabel('\delta_c', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
ylabel('M_c', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman')
plot(sc_data_dc, sc_data_Mc, 'rs');
grid on
legend('4th Order RK', 'NACA 1135 Data')

% end
