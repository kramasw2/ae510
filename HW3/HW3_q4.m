clc
clear all
close all

y = 0:0.03:1;

Rc = 10;

g = 1.4; % gamma
eta = 1; % eta


e = 1/(Rc + eta);

u1 = (1/2)*(y.^2) - 1/4; % u1 

v1 = (1/4)*(y.^3) - (1/4)*y;

u2 = (2*g + 9)*(y.^4)/24 - (4*g + 15 - 12*eta)*(y.^2)/24 ...    % u2
    + (10*g + 57 - 72*eta)/288;

v2 = (g + 3)*(y.^5)/9 - (20*g + 63 - 36*eta)*(y.^3)/96 ...      % v2
    + (28*g + 93 - 108*eta)*(y)/288;

u3 = (556*(g^2) + 1737*g + 3069)*(y.^6)/10368 ...                                      % u3         
    - (388*(g^2) + (1161 - 384*eta)*g + (1881 - 1728*eta))*(y.^4)/2304 ...
    + (304*(g^2) +(831 - 576*eta)*g + (1242 - 2160*eta + 864*(eta^2)))*(y.^2)/1728 ...
    - (2708*(g^2) + (7839 - 5760*eta)*g + (14211 - 32832*eta + 20736*(eta^2)))/82944;
    
v3 = (6836*(g^2) + 23031*g + 30627)*(y.^7)/82944 ...
    - (3380*(g^2) + (11391 - 3840*eta)*g + (15291 - 11520*eta))*(y.^5)/13824 ...
    + (3424*(g.^2) + (11271 - 7200*eta)*g + (15228 - 22860*eta + 6480*(eta^2)))*(y.^3)/13824 ...
    - (7100*(g^2) + (22311 - 20160*eta)*g + (30249 - 66960*eta + 38880*(eta^2)))*(y)/82944;

%% First Order

M_1 = 1 + 0.5*(g + 1)*(u1*e);

%% Second Order

M_2 = M_1 + 0.5*(g + 1)*(u2 + 0.75*(g - 1)*(u1.^2))*(e^2);

%% Third order

M_3 = M_2 + 0.5*(g + 1)*(u3 + (g + 1)*(v1.^2)/4 + 1.5*(g - 1)*(u1.*u2) ...
    + (5*(g^2) - 8*g + 3)*(u1.^3)/8)*(e^3);

%% Plots

Image_M = figure;
axes1 = axes('Parent',Image_M,'FontSize',15,'FontName','Times New Roman');
hold(axes1, 'on');
plot(y, M_1, y, M_2, y, M_3);
legend('1^{st} order', '2^{nd} Order', '3^{rd} Order', 'Location', 'northwest');
xlabel('y', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
ylabel('M(y)','FontAngle','italic','FontSize',25,'FontName','Times New Roman');

graphtitle = sprintf('R_c = %d', Rc);

title(graphtitle, 'FontName', 'Times New Roman');

mvsy = zeros(34,4);
mvsy(:,1) = transpose(y);
mvsy(:,2) = transpose(M_1);
mvsy(:,3) = transpose(M_2);
mvsy(:,4) = transpose(M_3);

dM1 = sqrt(mean((M_1 - M_2).^2));
dM2 = sqrt(mean((M_2 - M_3).^2));




