function [delta_c, cp_c, M_c, ANSWERS, P02_P01] = HW4_q1(theta_s, dpsi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This Function computes the cone angle based an input consisting of 
% the freestream Mach number M1 and the conical shock angle, theta_s.
% It uses the secant method to determine the cone half angle.
% It also returns values of the local-to-freestream propoerties for 
% different values of psi. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Inputs

gam = 1.405;
M1 = 2.0;
theta_s = theta_s*pi/180;
dpsi = -dpsi*pi/180;


%% Initial Conditions
    
psi = theta_s:dpsi:0;       
psi = transpose(psi);               % column vector
v_psi = zeros(length(psi),1);       % v_psi is non-dimensionalized
v_r = zeros(length(psi),1);
M_star = @(M) ((0.5*(gam+1)*(M^2))./...
               (1 + 0.5*(gam - 1)*(M^2)))^0.5;

% Initializing v_psi

v_psi(1,1) = - M_star(M1)*sin(theta_s)*...
            (1-(2/(gam+1))*((M1*sin(theta_s))^2 - 1)/(M1*sin(theta_s))^2);

% Initializing v_r
v_r(1,1) = M_star(M1)*cos(theta_s);

%% Numerical Solution Using 4th Order Runge-Kutta

% Defining functions based on governing equations
f = @(j,m,n) n;                             % j = psi, m = v_r, n = v_psi
g = @(j,m,n) ((0.5*(gam + 1) - 0.5*(gam - 1)*...
             (m^2 + n^2)))*...
    (m + n*cot(j))./...
    (n^2 - ((0.5*(gam + 1) - 0.5*(gam - 1)*...
             (m^2 + n^2)))) - m;  
i = 1;

% Numerical integration loop
    while v_psi(i) < 0
        
        k_1 = f(psi(i), v_r(i), v_psi(i));

        l_1 = g(psi(i),v_r(i), v_psi(i));

        k_2 = f(psi(i),v_r(i), v_psi(i) + l_1*dpsi*0.5);

        l_2 = g(psi(i)+0.5*dpsi,v_r(i)+0.5*dpsi*k_1,....
            v_psi(i)+ 0.5*dpsi*l_1);

        k_3 = f(psi(i),v_r(i), v_psi(i) + l_2*dpsi*0.5);

        l_3 = g(psi(i)+0.5*dpsi,v_r(i)+0.5*dpsi*k_2,...
            v_psi(i) + 0.5*dpsi*l_2);

        k_4 = f(psi(i),v_r(i), v_psi(i) + l_3*dpsi);

        l_4 = g(psi(i)+dpsi,v_r(i)+k_3*dpsi,...
            v_psi(i) + l_3*dpsi);

        v_r(i+1) = v_r(i) + (1/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)*dpsi;
        v_psi(i+1) = v_psi(i) + (1/6)*(l_1 + 2*l_2 + 2*l_3 + l_4)*dpsi;
        
        i = i+1;
    end

v_psi = v_psi(v_psi~=0); % Truncate values beyond cone surface (except
                         % the one nearest to zero
                         
psi = psi(1:i,1);        % corresponding psi, v_r values
v_r = v_r(1:i, 1);

%% Delta_c calculations using the Secant method 

j = i-1;        % Iterations begin at value just less than zero   
done = 0;

% Run the integration again for the points near v_psi = 0
while done == 0
    slope = (psi(j+1) - psi(j))./(v_psi(j+1) - v_psi(j));
    dpsi = slope*(- v_psi(j));
    while v_psi(j) < 0
        
        k_1 = f(psi(j), v_r(j), v_psi(j));

        l_1 = g(psi(j),v_r(j), v_psi(j));

        k_2 = f(psi(j),v_r(j), v_psi(j) + l_1*dpsi*0.5);

        l_2 = g(psi(j)+0.5*dpsi,v_r(j)+0.5*dpsi*k_1,....
            v_psi(j)+ 0.5*dpsi*l_1);

        k_3 = f(psi(j),v_r(j), v_psi(j) + l_2*dpsi*0.5);

        l_3 = g(psi(j)+0.5*dpsi,v_r(j)+0.5*dpsi*k_2,...
            v_psi(j) + 0.5*dpsi*l_2);

        k_4 = f(psi(j),v_r(j), v_psi(j) + l_3*dpsi);

        l_4 = g(psi(j)+dpsi,v_r(j)+k_3*dpsi,...
            v_psi(j) + l_3*dpsi);

        v_r(j+1) = v_r(j) + (1/6)*(k_1 + 2*k_2 + 2*k_3 + k_4)*dpsi;
        v_psi(j+1) = v_psi(j) + (1/6)*(l_1 + 2*l_2 + 2*l_3 + l_4)*dpsi;
        
        psi(j+1) = psi(j) + dpsi;
        
        j = j+1;
    end
    j = j-1;
    if abs(v_psi(j)) < 1e-6     % Check tolerance for v_psi
        done = 1;               % Exit main loop
    end
    
end

psi = psi*180/pi;
delta_c = psi(end - 1);


%% Computing other parameters:

% M* values at different psi
M_star_vals = sqrt(v_psi.^2 + v_r.^2);

% M values at different psi
M = sqrt((2/(gam+1)).*(M_star_vals.^2)./...
        (1 - (gam - 1).*(M_star_vals.^2)./(gam+1)));

% M2 values at different psi
M2_star = M_star_vals(1);

% P2/P1 Static Pressure Ratio
P2_P1 = 1 + (2*gam/(gam + 1))*((M1*sin(theta_s))^2 -1);

% P02/P01 Stagnation Pressure ratio
P02_P01 = (((1 + 0.5*(gam-1)*M(1).^2)/(1 + 0.5*(gam-1)*M1^2))^...
            (gam/(gam - 1)))*(P2_P1);

% P/P1 Local-to-freestream Static Pressure
P_P1 = (((1 + 0.5*(gam-1)*M1^2)./(1 + 0.5*(gam - 1)*M.^2)).^...
         (gam/(gam - 1)))*(P02_P01);                         

% T/T1 Local-to-freestream Static Temperature
T_T1 = ((1 + 0.5*(gam-1)*M1^2)./(1 + 0.5*(gam - 1)*M.^2));

% rho/rho1 Local-to-freestream Static Density
rho_rho1 = P_P1./T_T1;
    
% cp at each psi
cp = (P_P1 - 1)/(0.5*gam*M1^2);


%% Computing parameters at the cone surface

M_c = M(end -1);        % Mach number at Cone
cp_c = cp(end - 1);     % Cp at Cone surface

[rws, col] = size(psi);
ANSWERS = zeros(rws,8);
ANSWERS(:,1) = psi;
ANSWERS(:,2) = v_psi;
ANSWERS(:,3) = v_r;
ANSWERS(:,4) = M_star_vals;
ANSWERS(:,5) = M;
ANSWERS(:,6) = P_P1;
ANSWERS(:,7) = T_T1;
ANSWERS(:,8) = rho_rho1;

end