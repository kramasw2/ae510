% function[] = inv_interior(inp)

%%%%%%%%%%%%%%%%%%      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inverse Interior Point Algorithm - HW6 Q1 - Karthik Ramaswamy  
% MOC analysis of unsteady, 1D Homentropic flow
% Knowing the location of the solution point in the current time step as
% well as the location and flow properties at the previous time step, find
% the flow properties at the current solution point as well as interpolated
% intersection locations of the characteristics shooting towards the
% previous time step.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
clear all;

%% Inputs

gam = 1.2;
delta = 0;
eps = 1e-4;

x3 = 0.125;
% Input matrix based on question format: x; t; u; P
ip = [0.1 0.125 0.15;
    0. 0. 0.;
    0.1665 0.2081 0.2498;
    0.1620 0.1606 0.1588];

t3 = 0.02262;

% Uncomment these two for the next two cases
% ip(3,:) = [1.1000 1.1200 1.1400];
% t3 = 0.01; % Cases 2 and 3

ip(3,:) = [-1.1000 -1.1200 -1.1400];
t3 = 0.01; % Cases 2 and 3

%% P-C Iterations

% Functions to define iteration parameters
l_plus = @(u, P, gam) 1/(u + P^(0.5*(gam - 1)/gam));    %Lambda_ plus
l_minus = @(u, P, gam) 1/(u - P^((gam - 1)/(2*gam)));   %Lambda_minus

Q = @(P, gam) gam*(P)^((gam + 1)/(2*gam));  % Q+, Q-

x_1 = @(l) x3 - t3/l;   % x_1 in terms of x3, t3
x_2 = @(l) x3 - t3/l;   % x_2 in terms of x3, t3

% Initializing u+, u-, P+, P-
u_plus = ip(3,2);
u_minus = ip(3,2);
P_plus = ip(4,2);
P_minus = ip(4,2);

temp1 = 0;
temp2 = 0;

%% PREDICTOR STEP

while true

x1 = x_1(l_plus(u_plus, P_plus, gam));
x2 = x_2(l_minus(u_minus, P_minus, gam));

% If x1 is between 4,5 and x2 is between 5,6
if (ip(1,1) < x1) &&(x1 < ip(1,2)) && (ip(1,2) < x2) &&(x2 < ip(1,3))
u1 = ip(3,2) + (x1 - ip(1,2))*(ip(3,1) - ip(3,2))/(ip(1,1) - ip(1,2));
u2 = ip(3,3) + (x2 - ip(1,3))*(ip(3,2) - ip(3,3))/(ip(1,2) - ip(1,3));
P1 = ip(4,2) + (x1 - ip(1,2))*(ip(4,1) - ip(4,2))/(ip(1,1) - ip(1,2));
P2 = ip(4,3) + (x2 - ip(1,3))*(ip(4,2) - ip(4,3))/(ip(1,2) - ip(1,3));
end

% If x1,x2 both between 4,5
if (ip(1,1) < x1) &&(x1 < ip(1,2)) && (ip(1,1) < x2) &&(x2 < ip(1,2))
u1 = ip(3,2) + (x1 - ip(1,2))*(ip(3,1) - ip(3,2))/(ip(1,1) - ip(1,2));
u2 = ip(3,2) + (x2 - ip(1,2))*(ip(3,1) - ip(3,2))/(ip(1,1) - ip(1,2));
P1 = ip(4,2) + (x1 - ip(1,2))*(ip(4,1) - ip(4,2))/(ip(1,1) - ip(1,2));
P2 = ip(4,2) + (x2 - ip(1,2))*(ip(4,1) - ip(4,2))/(ip(1,1) - ip(1,2));    
end

% If x1,x2 both between 5,6
if (ip(1,2) < x1 && x1 < ip(1,3) && ip(1,2) < x2 && x2< ip(1,3))
u1 = ip(3,3) + (x1 - ip(1,3))*(ip(3,2) - ip(3,3))/(ip(1,2) - ip(1,3));
u2 = ip(3,3) + (x2 - ip(1,3))*(ip(3,2) - ip(3,3))   /(ip(1,2) - ip(1,3));
P1 = ip(4,3) + (x1 - ip(1,3))*(ip(4,2) - ip(4,3))/(ip(1,2) - ip(1,3));
P2 = ip(4,3) + (x2 - ip(1,3))*(ip(4,2) - ip(4,3))/(ip(1,2) - ip(1,3));
end

u_plus = u1;
P_plus = P1;
u_minus = u2;
P_minus = P2;

if (abs((x1 - temp1)/temp1) <1e-4) && (abs((x2 - temp2)/temp2) < 1e-4)
    break
end

temp1 = x1;
temp2 = x2;
end

x_plus = temp1;
x_minus = temp2;

% Define Q_plus and Q_minus
Q_plus = Q(P_plus, gam);
Q_minus = Q(P_minus, gam);

% Solve simultaneously to get P3p, u3p
P3p = Q_minus*(P1 + Q_plus*(u1 + (P2 - Q_minus*u2)/Q_minus))/...
    (Q_plus + Q_minus);

u3p = (P3p - P2 + Q_minus*u2)/Q_minus;

% End of predictor

%% CORRECTOR ITERATIONS

u3 = zeros(5,1);
P3 = zeros(5,1);

u3(1,1) = u3p;
P3(1,1) = P3p;

% Initialize u_plus, u_minus based on predictor
u_plus = 0.5*(u1 + u3p);
P_plus = 0.5*(P1 + P3p);
u_minus = 0.5*(u2 + u3p);
P_minus = 0.5*(P2 + P3p);


temp1 = 0;
temp2 = 0;

for i = 1:length(u3)

while true

    x1 = x_1(l_plus(u_plus, P_plus, gam));  
    x2 = x_2(l_minus(u_minus, P_minus, gam));

    % If x1 is between 4,5 and x2 is between 5,6
    if (ip(1,1) < x1) &&(x1 < ip(1,2)) && (ip(1,2) < x2) &&(x2 < ip(1,3))
    u1 = ip(3,2) + (x1 - ip(1,2))*(ip(3,1) - ip(3,2))/(ip(1,1) - ip(1,2));
    u2 = ip(3,3) + (x2 - ip(1,3))*(ip(3,2) - ip(3,3))/(ip(1,2) - ip(1,3));
    P1 = ip(4,2) + (x1 - ip(1,2))*(ip(4,1) - ip(4,2))/(ip(1,1) - ip(1,2));
    P2 = ip(4,3) + (x2 - ip(1,3))*(ip(4,2) - ip(4,3))/(ip(1,2) - ip(1,3));
    end

    % If x1,x2 both between 4,5
    if (ip(1,1) < x1) &&(x1 < ip(1,2)) && (ip(1,1) < x2) &&(x2 < ip(1,2))
    u1 = ip(3,2) + (x1 - ip(1,2))*(ip(3,1) - ip(3,2))/(ip(1,1) - ip(1,2));
    u2 = ip(3,2) + (x2 - ip(1,2))*(ip(3,1) - ip(3,2))/(ip(1,1) - ip(1,2));
    P1 = ip(4,2) + (x1 - ip(1,2))*(ip(4,1) - ip(4,2))/(ip(1,1) - ip(1,2));
    P2 = ip(4,2) + (x2 - ip(1,2))*(ip(4,1) - ip(4,2))/(ip(1,1) - ip(1,2));    
    end

    % If x1,x2 both between 5,6
    if (ip(1,2) < x1 && x1 < ip(1,3) && ip(1,2) < x2 && x2< ip(1,3))
    u1 = ip(3,3) + (x1 - ip(1,3))*(ip(3,2) - ip(3,3))/(ip(1,2) - ip(1,3));
    u2 = ip(3,3) + (x2 - ip(1,3))*(ip(3,2) - ip(3,3))/(ip(1,2) - ip(1,3));
    P1 = ip(4,3) + (x1 - ip(1,3))*(ip(4,2) - ip(4,3))/(ip(1,2) - ip(1,3));
    P2 = ip(4,3) + (x2 - ip(1,3))*(ip(4,2) - ip(4,3))/(ip(1,2) - ip(1,3));
    end

    u_plus = 0.5*(u1 + u3(i,1));
    P_plus = 0.5*(P1 + P3(i,1));
    u_minus = 0.5*(u2 + u3(i,1));
    P_minus = 0.5*(P2 + P3(i,1));

    % Testing for convergence for x1, x2
    if (abs((x1 - temp1)/temp1) < 1e-4) && (abs((x2 - temp2)/temp2) < 1e-4)
        break
    end

    temp1 = x1;
    temp2 = x2;
end
    % Evaluate New Q+ Q- based on new u,P +-
    Q_plus = Q(P_plus, gam);
    Q_minus = Q(P_minus, gam);
    
    % Define new P3, u3 based on new  Q+-
    P3(i+1,1) = Q_minus*(P1 + Q_plus*(u1 + (P2 - Q_minus*u2)/Q_minus))/...
        (Q_plus + Q_minus);

    u3(i+1,1) = (P3(i+1,1) - P2 + Q_minus*u2)/Q_minus;
        u_plus = 0.5*(u1 + u3(i+1,1));
        P_plus = 0.5*(P1 + P3(i+1,1));
        u_minus = 0.5*(u2 + u3(i+1,1));
        P_minus = 0.5*(P2 + P3(i+1,1));
    
    % Check for convergence for u3, P3
    if (abs((u3(i+1,1) - u3(i,1))/u3(i,1)) < 1e-4)    &&...
            (abs((P3(i+1,1) - P3(i,1))/P3(i,1)) < 1e-4)
        break
    end
end
