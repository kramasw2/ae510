clear all;
close all;

tsteps = 500;
K = 1;

x = linspace(0,1, 51)';
A = 1 + 10*(x - 0.5).^2;
dA = 2*10.*(x - 0.5);
gam = 1.4;

% Initialize values at t = 0
rho(:,1) = linspace(0.985999, 0.01, 51)';
u(:,1) = linspace(0.183697, 2.0, 51)';
P(:,1) = linspace(0.980454, 0.01, 51)';
T(:,1) = P(:,1)./rho(:,1);

% Analytical Solution Stuff

% Root finding
for j = 1:51
    r = roots([.008 0 .12 0 .6 -A(j)/((.5*(gam+1))^(-.5*(gam+1)/(gam-1))) 1]);
    s(j,:) = r(5:6)';
end

M_an(1:26) = s(1:26,2);
M_an(27:51) = s(27:51,1);

P_an = (1+.5*(gam-1)*M_an.^2).^(-gam/(gam-1));
rho_an = P_an.^(1/gam);
a_an = sqrt(.5*(gam+1)*P_an./rho_an);
u_an = a_an.*M_an;
T_an = (1 + 0.5*(gam - 1)*M_an.^2).^(-1);

%% ANALYTICAL, INITIAL PLOTS
% u plot: analytical, initial
figure(1)
plot(x,u_an,'.r')
hold on;
plot(x,u(:,1),'--black');

% P plot: analytical, initial
figure(2)
plot(x,P_an,'.r')
hold on;
plot(x,P(:,1),'--black');

% rho plot: analytical, initial
figure(3)
plot(x,rho_an,'.r')
hold on;
plot(x,rho(:,1),'--black');
hold on 

%% PREDICTOR - CORRECTOR ITERATIONS USING MACCORMACK'S METHOD
deltax = 0.02;

for n = 1:tsteps
    % Initializing values of rho, u and P at inlet
    rho(1,n+1) = rho(1,1);
    u(1,n+1) = u(1,1);
    P(1,n+1) = P(1,1);
    
%     % Initializing predictor values at inlet (These aren't used)
%     rho_p(1,n+1) = rho(1,1);
%     u_p(1,n+1) = u(1,1);
%     P_p(1,n+1) = P(1,1);

    % CFL Stability condition implementation
    a = sqrt(0.5*(gam + 1).*(P(:,n)./rho(:,n)));
    deltat = min(K*deltax./(u(:,n) + a));
    
    for i = 2:(length(rho(:,n)) - 1)
        % Finding Predicted rho
        % drho_p just denotes the value of drho/dt at point i, at t = n;
        % rho_p is the predictor value at t = n+1;
        drho_p(i,n) = -(u(i,n)*(rho(i+1, n) - rho(i,n))/deltax +...
            rho(i,n)*(u(i+1,n) - u(i,n))/deltax + ...
            rho(i,n)*u(i,n)*dA(i)/A(i));
        rho_p(i, n+1) = rho(i,n) + deltat*drho_p(i,n);
        
        % Predicted u
        % Similarly for u
        du_p(i,n) = -(((gam + 1)/(2*gam)*(P(i+1,n) - P(i,n)/(rho(i,n)*deltax)) +...
            u(i,n)*(u(i+1,n) - u(i,n))/deltax));
        u_p(i, n+1) = u(i,n) + deltat*(du_p(i,n));
        
        % Predicted P
        % Similarly for P
        dP_p(i,n) = -(u(i,n)*(P(i+1,n) - P(i,n))/deltax +...
            gam*P(i,n)*(u(i+1,n) - u(i,n))/deltax +...
            gam*u(i,n)*P(i,n)*dA(i)/A(i));
        P_p(i,n+1) = P(i,n) + deltat*dP_p(i,n);
    end
    % Extrapolating values of rho_p at point 51
        rho_p(51,n+1) = 2*rho_p(50,n+1) - rho_p(49,n+1);
        u_p(51,n+1) = 2*u_p(50,n+1) - u_p(49,n+1);
        P_p(51,n+1) = 2*P_p(50,n+1) - P_p(49,n+1);
    
    for i = 2:(length(rho(:,n)) - 1)
        % Finding corrected rho
        % drho_c is the time deriv at i, at t = n+1 using backward diff.
        drho_c(i,n+1) = -(u_p(i,n+1)*(rho_p(i, n+1) - rho_p(i-1,n+1))/deltax +...
            rho_p(i,n+1)*(u_p(i,n+1) - u_p(i-1,n+1))/deltax + ...
            rho_p(i,n+1)*u_p(i,n+1)*dA(i)/A(i));
        rho(i, n+1) = rho(i,n) + deltat*(0.5)*(drho_p(i,n) + drho_c(i,n+1));
        % Finding Corrected u
        du_c(i,n+1) = -(((gam + 1)/(2*gam))*(P_p(i,n+1) - P_p(i-1,n+1)/(rho_p(i,n+1)*deltax)) +...
            u_p(i,n+1)*(u_p(i,n+1) - u_p(i-1,n+1))/deltax);
        u(i, n+1) = u(i,n) + deltat*(0.5)*(du_p(i,n) +du_c(i,n+1));
        % Finding Corrrected P
        dP_c(i,n+1) = -(u_p(i,n+1)*(P_p(i,n+1) - P_p(i-1,n+1))/deltax +...
            gam*P_p(i,n+1)*(u_p(i,n+1) - u_p(i-1,n+1))/deltax +...
            gam*u_p(i,n+1)*P_p(i,n+1)*dA(i)/A(i));
        P(i, n+1) = P(i,n) + deltat*(0.5)*(dP_p(i,n) + dP_c(i,n+1));
    end
    % Extrapolating value of rho, u, P at exit
    rho(51,n+1) = 2*rho(50,n+1) - rho(49,n+1);
    u(51,n+1) = 2*u(50,n+1) - u(49,n+1);
    P(51,n+1) = 2*P(50,n+1) - P(49,n+1);
    T(:,n+1) = P(:,n+1)./rho(:,n+1);
    
    % Plotting for t = 500
    if n == 500
        figure(1)
        plot(x,u(:,n),'-black');
        hold on
        ylim([0 2.5])  
        str1='$$\overline{\textbf x}$$';
        xlabel(str1,'interpreter','latex','FontSize',25,'FontName','Times New Roman');
        str2='$$\overline{\textbf u}$$';
        ylabel(str2,'interpreter','latex','FontSize',25,'FontName','Times New Roman');
        title(str2,'interpreter','latex');
        legend('Analytical Solution', 'I.V. time plane', 'Solution at t = 500');
        grid on
        
        figure(2)
        h = plot(x,P(:,n));
        hold on 
        str1='$$\overline{\textbf x}$$';
        xlabel(str1,'interpreter','latex','FontSize',25,'FontName','Times New Roman');
        str2='$$\overline{\textbf P}$$';
        ylabel(str2,'interpreter','latex','FontSize',25,'FontName','Times New Roman');
        title(str2,'interpreter','latex');
        legend('Analytical Solution', 'I.V. time plane', 'Solution at t = 500');
        grid on
    end
end
