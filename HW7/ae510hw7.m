clear 
close all


N = 51;

gamma = 1.4;

K = 1;

data_i=1;

rho = linspace(.985999,.01,51)';
u = linspace(.183697,2.0, 51)';
p = linspace(.980454, .01, 51)';

g(:,:) = [rho u p]';

x = linspace(0,1,51)';

dx = 1/(N-1);


dAdx = 20*(x-.5);
A = 1+10*(x-.5).^2;
dgdx(3,N-1)=0;


%Analytical Solution
c = (.5*(gamma+1))^(-.5*(gamma+1)/(gamma-1));

for k = 1:N
    r = roots([.008 0 .12 0 .6 -A(k)/c 1]);
    s(k,:) = r(5:6)';
end

M_an(1:26) = s(1:26,2);
M_an(27:51) = s(27:51,1);

p_an = (1+.5*(gamma-1)*M_an.^2).^(-gamma/(gamma-1));
rho_an = p_an.^(1/gamma);
a_an = sqrt(.5*(gamma+1)*p_an./rho_an);
u_an = a_an.*M_an;

plot(x,u_an,'or')
%plot(x,p_an,'or')
hold on 

%Initial value
figure(1)
h = plot(x,g(2,:),'-.black');




for j=1:500
    
    rho = g(1,:)';
    u = g(2,:)';
    p = g(3,:)';
    a = sqrt(.5*(gamma+1)*p./rho);
    dt = min(K*dx./(u+a));
    
    %--------------Predictor------------------
    for i=1:N-1 %for all x
        dgdx(:,i) = (g(:,i+1)-g(:,i))/dx;
    end

    drhodx = dgdx(1,:)';
    dudx = dgdx(2,:)';
    dpdx = dgdx(3,:)';

    drhodt = - (u(1:N-1).*drhodx + rho(1:N-1).*dudx + rho(1:N-1).*u(1:N-1).*dAdx(1:N-1)./A(1:N-1));
    dudt = - (.5*(gamma+1)*dpdx./(rho(1:N-1)*gamma) + u(1:N-1).*dudx);
    dpdt = - (u(1:N-1).*dpdx + gamma*p(1:N-1).*dudx + gamma*u(1:N-1).*p(1:N-1).*dAdx(1:N-1)./A(1:N-1));
    dgdt(:,:) = [drhodt dudt dpdt]';

    g_p = g(:,1:N-1) + dgdt*dt; 

    % right boundary
    g_p(:,N) = 2*g_p(:,N-1)-g_p(:,N-2);

    % ------------Corrector-------------
    dgdx_p(3,N-2)=0;
    i=0;
    for i=1:N-2 %for all x
        dgdx_p(:,i) = (g_p(:,i+1)-g_p(:,i))/dx;
    end

    drhodx = dgdx_p(1,:)';
    dudx = dgdx_p(2,:)';
    dpdx = dgdx_p(3,:)';

    drhodt = - (u(2:N-1).*drhodx + rho(2:N-1).*dudx + rho(2:N-1).*u(2:N-1).*dAdx(2:N-1)./A(2:N-1));
    dudt = - (.5*(gamma+1)*dpdx./(rho(2:N-1)*gamma) + u(2:N-1).*dudx);
    dpdt = - (u(2:N-1).*dpdx + gamma*p(2:N-1).*dudx + gamma*u(2:N-1).*p(2:N-1).*dAdx(2:N-1)./A(2:N-1));
    dgdt_p(:,:) = [drhodt dudt dpdt]';

    g_c(3,N)=0;
    g_c(:,2:N-1) = g(:,2:N-1) + .5*(dgdt(:,2:N-1)+dgdt_p)*dt; 
    g_c(:,1) = g(:,1);

    % right boundary
    g_c(:,N) = 2*g_c(:,N-1)-g_c(:,N-2);  
    
    g=g_c;
    
    figure(1)
    if mod(j,500)==0
        %h = plot(x,g_c(2,:),'black');
        h = plot(x,g_c(2,:),'black');
        hold on
        % h = plot(x,g_c(1,:));
         ylim([0 2.5])  
        str='$$\overline{\textbf x}$$';
        xlabel(str,'interpreter','latex');
        str='$$\overline{\textbf u}$$';
        ylabel(str,'interpreter','latex');
        title(str,'interpreter','latex');
    end
     
%     if mod(j,100)==0
%         udata(data_i)=g(2,:);
%         pdata(data_i)=g(3,:);
%         rhodata(data_i)=g(1,:);
        
        end
end
 grid on
 grid minor
 legend('analytical','initial','final') 
 
