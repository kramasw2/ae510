function[x_3, y_3, u_3, v_3, iter] = HW5A_q1(gam, delta, eps, Input)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Interior Point Algorithm - HW 5A, AE 510 - Karthik Ramaswamy         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input = [0.47266 0.25369 1.1672 0.028314; 0.46192 0.0 1.1462 0.0];

%% Inputs

gam = 1.4;
delta = 1;
eps = 1e-4;

% Initializations
solution = zeros(100,4);    % x3, y3, u3, v3
y_plus = zeros(100,1);     
y_minus = zeros(100,1);
u_plus = zeros(100,1);
u_minus = zeros(100,1);
v_plus = zeros(100,1);
v_minus = zeros(100,1);
lambda = zeros(100,2); % lambda(i,1) = lambda_plus, (i,2) = lambda_minus
Q = zeros(100,2);      % Q(i,1) = Q_minus, Q(i,2) = Q_plus
R = zeros(100,2);      % R(i,1) = R_minus, R(i,2) = R_plus
S = zeros(100,2);      % S(i,1) = S_minus, S(i,2) = S_plus

% Point Inputs
x_1  = Input(1,1);
x_2 = Input(2,1);
y_1 = Input(1,2);
y_2 = Input(2,2);
u_1 = Input(1,3);
u_2 = Input(2,3);
v_1 = Input(1,4);
v_2 = Input(2,4);

%% Functions to define parts of the sol

% Mach Numbers
M_star = @(u,v) sqrt(u^2 + v^2);
M = @(M_star) sqrt((2*(M_star)^2)/((gam+1)*(1 - (gam - 1)*...
    ((M_star)^2)/(gam + 1))));

% Theta, Alpha
alpha = @(M) asin(1/M);
theta = @(r,s) atan(s/r);

% Equation Parameters
lambda_minus = @(f,h) tan(theta(f,h) - alpha(M(M_star(f,h))));
lambda_plus = @(f,h) tan(theta(f,h) + alpha(M(M_star(f,h))));
Q_fun = @(l,p) l^2 - 0.5*(gam + 1) + 0.5*(gam - 1)*...
            ((M_star(l,p))^2);

R_minus = @(k,l) 2*k*l - Q_fun(k,l)*lambda_minus(k,l);        
R_plus = @(k,l) 2*k*l - Q_fun(k,l)*lambda_plus(k,l);

S_fun = @(u,v,y) delta*(0.5*(gam + 1) - 0.5*(gam - 1)*...
                (M_star(u,v))^2)*v/y;
    
%% Predictor-Corrector iteration method


u_minus(1,1) = u_1;
u_plus(1,1) = u_2;
v_minus(1,1) = v_1;
v_plus(1,1) = v_2;
y_minus(1,1) = y_1;
y_plus(1,1) = y_2;

i = 1;


    while i < 99
       % Predictor step and iteration
       lambda(i,1) = lambda_minus(u_minus(i,1), v_minus(i,1));
       lambda(i,2) = lambda_plus(u_plus(i,1), v_plus(i,1));
       Q(i,1) = Q_fun(u_minus(i,1), v_minus(i,1));
       Q(i,2) = Q_fun(u_plus(i,1), v_plus(i,1));
       R(i,1) = R_minus(u_minus(i,1), v_minus(i,1));
       R(i,2) = R_plus(u_plus(i,1), v_plus(i,1));
       S(i,1) = S_fun(u_minus(i,1), v_minus(i,1), y_minus(i,1));
       S(i,2) = S_fun(u_plus(i,1), v_plus(i,1), y_plus(i,1));
       
        if isnan(abs(S(1,2))) == 1
        S(1,2) = S_fun(u_plus(1,1), v_minus(1,1), y_minus(1,1));
        end

       solution(i+1,2)   = (-lambda(i,1)*lambda(i,2)*(x_1 - x_2) +...
           lambda(i,2)*y_1 - lambda(i,1)*y_2)/...
           (lambda(i,2) - lambda(i,1));
       solution(i+1,1) = (solution(i+1,2) - y_1)/(lambda(i,1)) + x_1;
       
       solution(i+1,4) = (Q(i,2)*Q(i,1)*(u_1 - u_2) + R(i,1)*Q(i,2)*v_1...
           - R(i,2)*Q(i,1)*v_2 + S(i,1)*Q(i,2)*(solution(i+1,1) - x_1) -...
           Q(i,1)*S(i,2)*(solution(i+1,1) - x_2))/...
           (R(i,1)*Q(i,2) - R(i,2)*Q(i,1));
       
       solution(i+1,3) = (Q(i,2)*u_2 + R(i,2)*v_2 +S(i,2)*...
           (solution(i+1,1) - x_2) - R(i,2)*solution(i+1,4))/Q(i,2); 
       
%        syms x3 y3
%        [solution(i+1,1), solution(i+1,2)] = solve(lambda(i,1)*x3 - y3 ==...
%                     lambda(i,1)*x_1 - y_1, lambda(i,2)*x3 - y3 ==...
%                     lambda(i,2)*x_2 - y_2, x3, y3);
%        syms u3 v3
%        [solution(i+1,3), solution(i+1,4)] = solve(Q(i,1)*u3 + R(i,1)*v3 ==...
%            Q(i,1)*u_1 + R(i,1)*v_1 + S(i,1)*(solution(i,1) - x_1),...
%            Q(i,2)*u3 + R(i,2)*v3 ==...
%            Q(i,2)*u_2 + R(i,2)*v_2 + S(i,2)*(solution(i,1) - x_2), u3, v3);
       
       
       % 'Corrector' Step
       u_minus(i+1,1) = 0.5*(u_minus(1,1) + solution(i+1,3));
       u_plus(i+1,1) = 0.5*(u_plus(1,1) + solution(i+1,3));
       v_minus(i+1,1) = 0.5*(v_minus(1,1) + solution(i+1,4));
       v_plus(i+1,1) = 0.5*(v_plus(1,1) + solution(i+1,4));
       y_minus(i+1,1) = 0.5*(y_minus(1,1) + solution(i+1,2));
       y_plus(i+1,1) = 0.5*(y_plus(1,1) + solution(i+1,2));   
       if abs((solution(i+1,1) - solution(i,1))/solution(i+1,1)) < eps...
         && abs((solution(i+1,2) - solution(i,2))/solution(i+1,2)) < eps...
         && abs((solution(i+1,3) - solution(i,3))/solution(i+1,3)) < eps...
         && abs((solution(i+1,4) - solution(i,4))/solution(i+1,4)) < eps
               break
       end
       i = i + 1;
    end
iter = i -1;
% Comment out these lines to makes this into a function

x_3 = solution(i,1);
y_3 = solution(i,2);
u_3 = solution(i,3);
v_3 = solution(i,4);

