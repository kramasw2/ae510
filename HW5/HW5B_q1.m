%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AE 510 HW 5B - Karthik Ramaswamy -  Axisymmetric Nozzle case    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc;
clear all;
close all;

%% Input
R_c = 2.0;
gam = 1.4;
eps = 1e-4;
delta = 1;


%Mach Number and Pressure Ratio
M = @(u,v)sqrt(u^2 + v^2);
P_ratio = @(M) (1 + 0.5*(gam - 1)*M.^2).^(gam/(1-gam));


% Input data for initial value line
iv_line = importdata('Initial value table_HW5B.csv',','); 


% Structure for solution points
s = struct('x',[],'y',[],'u',[],'v',[], 'iter', []);  

s.x(:,1) = iv_line.data(:,1);
s.y(:,1) = iv_line.data(:,2);
s.u(:,1) = iv_line.data(:,3);
s.v(:,1) = iv_line.data(:,4);
s.mach(:,1) = sqrt(s.u(:,1).^2 + s.v(:,1).^2);
s.p_ratio(:,1) = P_ratio(s.mach(:,1));


j = 1;
count = length(s.x(:,1));

%% Based on the number of points given in the input

while j < 2*length(s.x(:,1)) - 1
    
    for i = 1:(count -1)      

    %       Conditions for implementing the AoS point algorithm            
             if mod(j,2) == 0  % IF j is even, implement AoS as well as IP
                Input = [s.x(i+1,j) s.y(i+1,j) s.u(i+1, j) s.v(i+1,j);...
                s.x(i,j) s.y(i,j) s.u(i,j) s.v(i,j)];
                if i == 1

                [s.x(i,j+1),s.y(i,j+1), s.u(i,j+1), ...
                s.v(i,j+1), s.iter(i, j+1)] = aospoint(gam, delta, eps, Input(2,:));
                s.mach(i,j+1) = M(s.u(i,j+1),s.v(i,j+1));
                s.p_ratio(i,j+1) = P_ratio(s.mach(i,j+1));
                
                end
                
                [s.x(i+1,j+1),s.y(i+1,j+1), s.u(i+1,j+1), ...
                s.v(i+1,j+1), s.iter(i+1, j+1)] = interiorpoint(gam, delta, eps, Input);
                s.mach(i+1,j+1) = M(s.u(i+1,j+1),s.v(i+1,j+1));
                s.p_ratio(i+1,j+1) = P_ratio(s.mach(i+1,j+1));
            
             else % if j is Odd, use IP for all points
                 Input = [s.x(i+1,j) s.y(i+1,j) s.u(i+1, j) s.v(i+1,j);...
                        s.x(i,j) s.y(i,j) s.u(i,j) s.v(i,j)];
                    
                    [s.x(i,j+1),s.y(i,j+1), s.u(i,j+1), ...
                    s.v(i,j+1), s.iter(i, j+1)] = interiorpoint(gam, delta, eps, Input);
                    s.mach(i,j+1) = M(s.u(i,j+1),s.v(i,j+1));
                    s.p_ratio(i,j+1) = P_ratio(s.mach(i,j+1));
             end

    end
    if s.y(1,j) == 0
        count = count - 1;
    end
    j = j+1;
end


% To Calculate the last point based on the AoS algorithm (single input
% necessary, which means only AoS Point will work) This is because the way
% I defined count, the last point has to be calculated explicitly

if count == 1 % When the calculation is complete but for one point
    j = j-1;
    i = 1;
    Input = [s.x(i+1,j) s.y(i+1,j) s.u(i+1, j) s.v(i+1,j);...
                s.x(i,j) s.y(i,j) s.u(i,j) s.v(i,j)];
     [s.x(i,j+1),s.y(i,j+1), s.u(i,j+1), ...
                s.v(i,j+1), s.iter(i, j+1)] = aospoint(gam, delta, eps, Input(2,:));
            s.mach(i,j+1) = M(s.u(i,j+1),s.v(i,j+1));
                    s.p_ratio(i,j+1) = P_ratio(s.mach(i,j+1));
end


            
%% Plotting

xyplot = figure;
for p = 1:j+1
    plot(s.x(:,p), s.y(:,p), 'ko','MarkerSize',3);
    hold on;
end
xlim([0.25 2]);
ylim([0 1.1]);
xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
ylabel('y', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
legend('Gridpoint')
grid 'on'



machno = figure;
for p = 1:j+1
    % Plot along B-C:
    plot(s.x(1,p), s.mach(1,p), 'rs','MarkerSize',3);
    hold on;
    % Plot along A-C:
    for k = 21:-1:1
        if s.mach(k,p) ~= 0
            plot(s.x(k,p), s.mach(k,p), 'bo', 'MarkerSize',3);
            hold on;
            break
        end
    end
end

xlabel('x', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
ylabel('M', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
legend('Mach Number Along B-C', 'Mach Number Along A-C', 'location', 'southeast');
grid 'on'


pratio = figure;
for p = 1:j+1
    % Plot along B-C:
    plot(s.x(1,p), s.p_ratio(1,p),'ro','MarkerSize',3);
    hold on;
    % Plot along A-C:
    for k = 21:-1:1
        if s.p_ratio(k,p) ~= 0
            plot(s.x(k,p), s.p_ratio(k,p), 'bs','MarkerSize',3);
            hold on;
            break
        end
    end
end

xlabel('x', 'FontAngle','italic','FontSize',25, 'FontName','Times New Roman');
ylabel('P/P_0', 'FontAngle','italic','FontSize',25,'FontName','Times New Roman');
legend('Pressure Ratio Along B-C', 'Pressure Ratio Along A-C');
grid 'on'

